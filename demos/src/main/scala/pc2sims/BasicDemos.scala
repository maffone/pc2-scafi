/*
 * Copyright (C) 2016-2017, Roberto Casadei, Mirko Viroli, and contributors.
 * See the LICENCE.txt file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

package pc2sims

import java.awt.Font

import it.unibo.scafi.incarnations.BasicSimulationIncarnation._
import it.unibo.scafi.incarnations.BasicSimulationIncarnation.{AggregateProgram, Builtins}
import it.unibo.scafi.simulation.gui.controller.Controller
import it.unibo.scafi.simulation.gui.Settings

trait BasicDemo extends AggregateProgram {
  def sense1 = sense[Boolean]("sens1")
  def sense2 = sense[Boolean]("sens2")
  def sense3 = sense[Boolean]("sens3")
  def boolToDouble(b: Boolean) = mux(b){1.0}{0.0}
  def boolToInt(b: Boolean) = mux(b){1}{0}
  def nbrRange = nbrvar[Double]("nbrRange")*100
  def random() = java.lang.Math.random()


  def main(args: Array[String]): Unit = {
    Settings.Sim_ProgramClass = this.getClass().getSuperclass.getName
    Settings.ShowConfigPanel = false
    Settings.Sim_NbrRadius = 0.15
    Settings.Sim_NumNodes = 100
    Settings.Size_Device_Relative = 120
    Settings.Font_Label = new Font("Arial", Font.BOLD, 30)
    Settings.To_String = (b: Any) => b match {
      case d:Double if d>10e6 => "inf"
      case d:Double => "%.2f".format(d)
      case x => x.toString
    }
    Controller.startup
  }

}

class Main extends BasicDemo {
  override def main() = 1
}
object MainApp extends Main
